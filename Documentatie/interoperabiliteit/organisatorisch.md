---
title: Organisatorische interoperabiliteit
weight: 2
---
## Gewenste momenten van aanlevering
De gegevensuitwisseling vindt plaats op het moment dat er een kostenonderzoek opgezet is. Dit is eens in de 3 tot 5 jaar. De laatste uitvraag dateert uit 2016. Het is nog niet duidelijk wanneer een volgend kostenonderzoek plaats zal vinden. Per kostenonderzoek is er één uitvraag die plaatsvindt nadat de jaarrekeningen zijn vastgesteld.  De doorlooptijd voor beantwoording van de uitvraag zal maximaal twee maanden zijn.
Voor mogelijke deel-kostenonderzoeken kan er gebruik gemaakt worden van de afspraken in dit basis uitwisselprofiel. Hierbij gaat het dan om een selectie van de informatievragen uit dit basisonderzoek. Wanneer er sprake is van nieuwe vragen in deze deelonderzoeken, gaat dit middels het beheerafspraken proces. 

## Gewenste moment van terugkoppeling
Individuele terugkoppeling vindt plaats in het geval dat aangeleverde gegevens niet compleet zijn of incorrect. In dat geval neemt de NZa contact op met de betreffende zorgaanbieder.
Het kostenonderzoek leidt eventueel tot aangepaste tarieven en/of beleidsregels. Na het vaststellen van de beleidsregels vindt geen individuele terugkoppeling meer plaats.

## Looptijd
De looptijd van het uitwisselprofiel is continu doorlopend tot het moment van wijziging.

## Gewenste bewaartermijn
Op dit moment bewaart de NZa de gegevens en antwoorden die de zorgaanbieders aanleveren ten behoeve van het kostenonderzoek. De gegevens worden bewaard zo lang de beleidsregels, die volgen uit het betreffende kostenonderzoek, in werking zijn. Hierbij wordt de wettelijke bewaartermijn in ogenschouw genomen van maximaal tien jaar. NB: Afhankelijk van de technische mogelijkheden en principes van KIK-V worden de afspraken rond de bewaartermijnen nog aangepast.

Het kostenonderzoek hoeft niet gereproduceerd te kunnen worden met de data van de zorgaanbieder. Wel doet de NZa nog een controle op de ingediende antwoorden. Het kan zijn dat de NZa een (verdiepende) vraag heeft over hetgeen is aangeleverd. Dit gebeurt meestal binnen enkele maanden na het indienen van de gegevens. Het is dan belangrijk dat de zorgaanbieder de data en haar gegevens nog tot haar beschikking heeft. De NZa vraagt de zorgaanbieder de gegevens in ieder geval een jaar te bewaren.

## Afspraken bij vragen over kostenonderzoek of over hetgeen wordt uitgevraagd 
Bij elk kostenonderzoek wordt aangegeven op welke wijze contact opgenomen kan worden en met wie bij vragen over het kostenonderzoek en hetgeen wordt uitgevraagd. Vragen kunnen over het algemeen worden gesteld per e-mail of door contact op te nemen met de helpdesk. Alle contactgegevens worden gedeeld in de communicatie wanneer de uitvraag wordt uitgezet.

## Afspraken bij twijfels over de kwaliteit van gegevens
De NZa neemt telefonisch of via mail contact op met zorgaanbieders die afwijkende data aanleveren. Het is dan mogelijk om nieuwe data aan te leveren. Mochten de afwijkingen blijven dan worden deze extreme waardes uitgesloten van het onderzoek. 

## In- en exclusiecriteria
Inclusie: Sector Wlz, 
Exclusie: Gehandicaptenzorg, GGZ, Wmo, Zvw.

Met inclusie voor de Wlz wordt bedoeld: alle zorgaanbieders waarbij minimaal één prestatie uit het Kwaliteitskader Verpleeghuiszorg geleverd wordt.
Dit kader heeft betrekking op de prestaties VV4-VV10. Zodra er dus één prestatie VV4-VV10 is geleverd, worden alle kosten en productie aangeleverd. Voor deze afbakening geldt dat er gekeken wordt naar zorg in natura (ZIN) en NIET naar pgb.
