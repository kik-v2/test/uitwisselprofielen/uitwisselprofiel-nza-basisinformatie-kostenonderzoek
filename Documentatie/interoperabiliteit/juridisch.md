---
title: Juridische interoperabiliteit
weight: 1
---
## Grondslag
Dit uitwisselprofiel kent zijn grondslag in de volgende wetsartikelen:

Wet Marktordening Gezondheidszorg:
* artikel 60
* artikel 61
* artikel 62
* artikel 63
* artikel 65
* artikel 68
* artikel 70

Zorgverzekeringswet:
* artikel 88
* artikel 89

Lees hiervoor ook de aanvullende toelichting in het juridisch kader, te vinden binnen de afsprakenset via [Juridisch kader](https://kik-v-publicatieplatform.nl/afsprakenset/_/content/docs/juridisch-kader)

In het kader van dit uitwisselprofiel worden geen persoonsgegevens uitgewisseld. De juridische grondslag benodigd voor het uitwisselen van persoonsgegevens is hier dus niet opgenomen.