---
title: Semantische interoperabiliteit
weight: 3
---
## Algemeen
|          **Vraag**                                                                                                                                                                                  |          **Doel en achtergrond**             |
|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------|
| Wat is het NZa-nummer van de organisatie?                                                                                                                           |           |
| Heeft de organisatie meerdere NZa codes?                                                                                                                                 |           |
| Indien ja, welke NZa codes zijn dit?                                                                                                                                   | De NZa-code is gekoppeld aan de nacalculatie 2016. Bij het invullen van alle NZa codes van de organisatie wordt de volledige productie in het kostenonderzoek meegenomen          |                    
| Wat is het KvK-nummer van de organisatie?                                                                                                                            | Het KvK-nummer wordt uitgevraagd om het financiele jaarverslag van de organisatie te koppelen aan de ingevulde gegevens.           |
| Wat is AGB code van de organisatie?                                                                                                                                  | Via de AGB-code(s) kan de NZa de Vektisdeclaraties koppelen aan het opgegeven NZa-code en zo de productieaantallen controleren.          |
| Contactgegevens                                                                                                                                  | Bij specifieke vragen over hetgeen is aangeleverd neemt de NZa contact op met deze contactpersoon.            |
| Functie                                                                                                                                                                  |                      |
| Naam contactpersoon                                                                                                                                                      |                      |
| Telefoonnummer                                                                                                                                                           |                      |
| E-mailadres                                                                                                                                                              |                      |


## Personeel
| **Vraag**                                                                                            |
|------------------------------------------------------------------------------------------------------|
| 1.1 Wat is het ziekteverzuimpercentage (excl. zwangerschapsverlof)?                                                     |
| 2.1 Wat is het aantal verloonde uren?                                              |
| 3.1 Wat zijn de kosten voor personeel in loondienst (PIL) exclusief ORT?                                                                 |
| 3.2 Wat is het gemiddelde bruto jaarsalaris per FTE?                                                               |
| 3.3 Wat zijn de totale loonkosten voor personeel in loondienst (PIL)? |
| 4.1 Wat zijn de sociale kosten?                                         |
| 4.2 Wat zijn de kosten voor personeel niet in loondienst (PNIL) kosten?                                                      |
| 4.3 Wat zijn de overige personeelskosten van personeel in loondienst (PIL)?                            |

**Doel en achtergrond**

1. De NZa vraagt ziekteverzuimpercentage uit om de productiviteit per functieprofiel te kunnen meten (om in een volgende fase de tarieven te kunnen bepalen).
2. De rest van de gegevens die worden uitgevraagd worden gebruikt als de bron van de kosten (de input van het kostprijsmodel).
                                                                                                                            
## Materiële kosten en kapitaallasten
| **Vraag**                                                                                            |
|------------------------------------------------------------------------------------------------------|
| 5.1 Wat zijn de kosten van voeding?                                                     |
| 5.2 Wat zijn andere hotelmatige kosten?                                              |
| 6.1 Wat zijn de vervoerskosten?                                                                   |
| 7.1 Wat zijn de algemene kosten?                                                              |
| 8.1 Wat zijn de client cq de bewonersgebonden kosten?  |
| 9.1 Wat zijn de terrein- en gebouwgebonden kosten?                                         |
| 9.2 Wat zijn de onderhoudskosten en dotaties aan voorzieningen groot onderhoud?                                                      |
| 9.3 Wat zijn de energiekosten (gas, elektra, water)?                           |
| 10.1 Wat zijn de afschrijvingen huur, leasing en interest?                            |
| 10.2 Wat zijn de kosten voor vervoersmiddelen en immateriële vaste activa?                         |
| 11.1 Wat zijn de kosten voor hulpbedrijven en overboekingsrekening?                           |
| 12.1 Wat is het toegekend wettelijk budget?                          |
| 12.2 Wat zijn de berekende correcties op het toegekend wettelijk budget?                           |
| 13.1 Wat zijn de overige bedrijfsopbrengsten?                            |
| 13.2 Wat zijn de eigen bijdragen cliënten?                          |
| 14.1 Wat zijn de correctie budgetafrekeningen voorgaande boekjaren?                            |
| 15.1 Wat zijn de totale opbrengsten (na correctie exploitatieresultaten)?                           |
| 16.1 Wat is het aantal gerealiseerde verblijfsdagen?                             |

**Doel en achtergrond**

1. Deze gegevens worden gebruikt als de bron van de kosten (de input van het kostprijsmodel).                                                          

## Algemene uitgangspunten
Voor de berekening van de indicatoren en informatievragen in de verschillende uitwisselprofielen worden algemene uitgangspunten gehanteerd. Uitgangspunten die gelden voor specifieke indicatoren of informatievragen worden bij de functionele beschrijving van de betreffende indicator beschreven. Indicator-specifieke uitgangspunten gaan voor op algemene uitgangspunten van een uitwisselprofiel. 

Voor alle uitwisselprofielen gelden onderstaande algemene uitgangspunten:

[Algemene uitgangspunten](https://kik-v-publicatieplatform.nl/documentatie/Algemene%20uitgangspunten)
