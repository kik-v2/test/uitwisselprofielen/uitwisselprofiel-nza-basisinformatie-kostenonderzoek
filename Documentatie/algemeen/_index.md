---
title: Algemeen
weight: 2
---
De Nederlandse Zorgautoriteit (hierna te noemen NZa) houdt toezicht op zorgaanbieders, zorgverzekeraars, zorgkantoren en CAK. Denk hierbij aan regels voor de betaalbaarheid en toegankelijkheid van de zorg. Hierbij analyseert de NZa risico’s en voeren zij onderzoeken uit. De NZa voert overleg met verschillende partijen om ervoor te zorgen dat mensen de zorg krijgen waar zij recht op hebben. Ook adviseert de NZa het ministerie van Volksgezondheid, Welzijn en sport (VWS).

Het uitwisselprofiel bevat de basisafspraken en basisinformatie die benodigd zijn voor eventueel toekomstig kostenonderzoek. Het is gebaseerd op eerder kostenonderzoek waaronder met name het kostenonderzoek dat in 2016 is uitgevoerd. Omdat nog niet duidelijk is of en wanneer er een (kosten)onderzoek gaat plaatsvinden, gaat het hier om een concept voor een mogelijk toekomstig uitwisselprofiel. Dit uitwisselprofiel is sec bedoeld voor pilot doeleinden. Op het moment dat bekend is dat er een kostenonderzoek zal plaatsvinden vanuit de NZa zal een nieuw uitwisselprofiel worden opgesteld en worden vastgesteld via de Ketenraad. In het nieuwe uitwisselprofiel wordt qua inhoud en informatievragen zo min mogelijk afgeweken van dit concept uitwisselprofiel.

## Doel van de uitvraag

Deze uitvraag is bedoeld om gegevens op te halen die nodig zijn voor het uitvoeren van een kostenonderzoek. Een kostenonderzoek levert inzichten over de productiekosten van goede verpleeghuiszorg zoals beschreven in het Kwaliteitskader. De bedoeling van deze uitvraag is om het geheel van kosten van de verpleeghuiszorg in beeld te brengen. 
