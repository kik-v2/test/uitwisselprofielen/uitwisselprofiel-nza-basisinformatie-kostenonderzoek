---
title: 9.2. Wat zijn de dotaties aan voorzieningen groot onderhoud?
description: "Het totaal aan dotaties aan voorzieningen groot onderhoud."
weight: 9
---
## Indicator

**Definitie:** Het totaal aan dotaties aan voorzieningen groot onderhoud.

**Teller:** Het totaal aan dotaties aan voorzieningen groot onderhoud.

**Noemer:** Niet van Toepassing.

## Toelichting

Deze indicator betreft het totaal aan onderhoudskosten en dotaties aan voorzieningen groot onderhoud over de periode.

De indicator wordt op organisatieniveau berekend.

Voor totaal aan onderhoudskosten en dotaties aan voorzieningen groot onderhoud hanteert de NZa het Prismant rekeningsschema (Bron: Kostenonderzoek Wet langdurige zorg NZa en KPMG — Versie 12 september 2017, pagina 18).

In het Prismant rekeningschema zijn dit alle rekeningen uit rekeninggroep 472 uit rubriek 47 (Bron: Rekeningschema-voor-zorginstellingen-Prismant - Utrecht, september 2004 Publicatienummer 203.25, pagina's 38 en 62). Deze rekeninggroep wordt gebruikt voor het doteren van egalisatievoorzieningen voor groot onderhoud die in de balans zijn opgenomen in rubriek 06 Voorzieningen groot onderhoud. Uitgaven in het kader van groot onderhoud worden ten laste van deze voorzieningen gebracht. Dergelijke onttrekkingen vinden niet via de exploitatierekening plaats maar kunnen in de toelichting bij de balanspost voorzieningen worden verantwoord.

De kosten van jaarlijks terugkerend onderhoud worden geboekt in de rekeninggroep 471.

Ter illustratie staan in onderstaande tabellen de verwijzingen naar de betreffende grootboekrekeningnummers conform het Referentiegrootboekschema (RGS) en het Prismant rekeningschema.

| Specificatie rubrieken RGS|
|----------------|
| WBedHuiDvg 4201230 Dotatie voorziening groot onderhoud gebouwen|

| Specificatie rubrieken Prismant|
|----------------|
| 472000 Dotaties aan voorziening groot onderhoud |

## Uitgangspunten

* Alle onderhoudskosten en dotaties aan voorzieningen groot onderhoud worden geincludeerd.

## Berekening

Deze indicator wordt als volgt berekend:

1. Tel alle boekingen van onderhoudskosten en dotaties aan voorzieningen groot onderhoud voor de betreffende meetperiode bij elkaar op.

| Periode:       | Totaal onderhoudskosten en dotaties aan voorzieningen groot onderhoud (Euro)|
|----------------|--------|
| Totaal organisatie | Stap 1 |
