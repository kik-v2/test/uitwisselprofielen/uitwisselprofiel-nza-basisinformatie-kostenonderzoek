---
title: 3.1. Loonkosten personeel in loondienst
description: "Totaal aan loonkosten inclusief vakantiegeld, vergoedingen (zoals eindejaarsuitkering) en sociale lasten"
weight: 3
---
## Indicator

**Definitie:** Totaal aan loonkosten inclusief vakantiegeld, vergoedingen (zoals eindejaarsuitkering) en sociale lasten.

**Teller:** Loonkosten PIL.

**Noemer:** Niet van toepassing.

## Toelichting

Deze indicator betreft de loonkosten van personeel in loondienst (PIL) over de periode inclusief vakantiegeld, vergoedingen (zoals eindejaarsuitkering) en sociale lasten. De indicator wordt op organisatieniveau berekend.

Voor loonkosten van personeel in loondienst (PIL) hanteert de NZa het Prismant rekeningsschema (Bron: Kostenonderzoek Wet langdurige zorg NZa en KPMG — Versie 12 september 2017, pagina 17).

Loonkosten van personeel in loondienst betreft de salariskosten van in dienstbetrekking verrichte arbeid op basis van een arbeidsovereenkomst. De instelling treedt op als werkgever en is uit dien hoofde inhoudingsplichtig voor de loonbelasting en premieheffing (Bron: Rekeningschema-voor-zorginstellingen-Prismant - Utrecht, september 2004 Publicatienummer 203.25, pagina 29).

Op grond van hiervan worden tot de salarissen gerekend:

* Brutosalaris (volgens inschalingtabellen) inclusief doorbetaalde salarissen tijdens ziekte;
* Vakantiebijslag;
* Vergoedingen voor overwerk, onregelmatige dienst, bereikbaarheids-, aanwezigheids- en
slaapdienst;
* Eindejaarsuitkeringen e.d. (Bron: Rekeningschema-voor-zorginstellingen-Prismant - Utrecht, september 2004 Publicatienummer 203.25, pagina 30).

Ter illustratie staan in onderstaande tabellen de verwijzingen naar de betreffende rubrieken conform het Referentiegrootboekschema (RGS) en het Prismant rekeningschema.

| Specificatie rubrieken RGS|
|----------------|
| WPerSol S.B 4002000 Sociale lasten |
| WPerLes S.A 4001000 Lonen en salarissen |

| Specificatie rubrieken Prismant*|
|----------------|
| 411 Algemene en administratieve functies |
| 412 Hotelfuncties |
| 413 Patiënt- c.q. bewonergebonden functies |
| 414 Leerling personeel |
| 415 Terrein- en gebouwgebonden functies |

## Uitgangspunten

* De kosten van alle personeelsleden in loondienst (PIL) worden geïncludeerd.

## Berekening

Deze indicator wordt als volgt berekend:

1. Tel alle boekingen van loonkosten van personeel in loondienst (PIL) voor de betreffende meetperiode bij elkaar op.

| Periode:       | Totaal directe loonkosten PIL (Euro)|
|----------------|--------|
| Totaal organisatie | Stap 1 |
