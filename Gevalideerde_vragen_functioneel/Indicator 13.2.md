---
title: 13.2. Wat zijn de eigen bijdragen cliënten? 
description: "Opbrengsten vanuit de eigen bijdrage van cliënten. Indien dit minder dan 2% van de totale opbrengsten betreft kan de invuller er ook voor kiezen om dit mee te nemen bij overige bedrijfsopbrengsten."
weight: 13
---
## Indicator

**Definitie:** Opbrengsten vanuit de eigen bijdrage van cliënten. Indien dit minder dan 2% van de totale opbrengsten betreft kan de invuller er ook voor kiezen om dit mee te nemen bij overige bedrijfsopbrengsten.

**Teller:** De eigen bijdragen cliënten.

**Noemer:** Niet van Toepassing.

## Toelichting

Deze indicator betreft de eigen bijdragen cliënten. De indicator wordt op organisatieniveau berekend.

Voor de eigen bijdragen cliënten hanteert de NZa de definitie volgens het Prismant rekeningsschema (Bron: Kostenonderzoek Wet langdurige zorg NZa en KPMG — Versie 12 september 2017, pagina 16).

Opbrengsten vanuit de eigen bijdrage van cliënten. Indien dit minder dan 2% van de totale opbrengsten betreft kan de invuller er ook voor kiezen om dit mee te nemen bij overige bedrijfsopbrengsten (indicator 13.1)

Ter illustratie staan in onderstaande tabellen de verwijzingen naar de betreffende grootboekrekeningnummers conform het Referentiegrootboekschema (RGS) en het Prismant rekeningschema.

| Specificatie rubrieken RGS |
|----------------|
| Nader uit te zoeken (verspreid door RGS aanwezig) |

| Specificatie rubrieken Prismant |
|----------------|
| 822000 Eigen bijdragen cliënten |

## Uitgangspunten

* Indien dit minder dan 2% van de totale opbrengsten betreft kan de invuller er ook voor kiezen om dit mee te nemen bij overige bedrijfsopbrengsten (indicator 13.1).

## Berekening

Deze indicator wordt als volgt berekend:

1. Tel alle boekingen van de eigen bijdragen cliënten voor de betreffende meetperiode bij elkaar op.

| Organisatieonderdeel       | Totaal eigen bijdragen cliënten (euro) |
|----------------|--------|
| Totaal organisatie | Stap 1 |
