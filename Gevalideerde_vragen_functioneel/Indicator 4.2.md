---
title: 4.2. Kosten personeel niet in loondienst (PNIL)
description: "Totale kosten van personeel niet in loondienst (PNIL)."
weight: 4
---
## Indicator

**Definitie:** Totale kosten van personeel niet in loondienst (PNIL).

**Teller:** Alle kosten van personeel niet in loondienst.

**Noemer:** Niet van Toepassing.

## Toelichting

Deze indicator betreft het totaal aan kosten van personeelsleden niet in loondienst (PNIL) over de periode. De indicator wordt op organisatieniveau berekend.

Voor kosten van personeel niet in loondienst (PNIL) hanteert de NZa het Prismant rekeningsschema (Bron: Kostenonderzoek Wet langdurige zorg NZa en KPMG — Versie 12 september 2017, pagina 17).

Kosten van personeel niet in loondienst (PNIL) betreft alle vergoedingen aan personeel dat niet in loondienst en als uitzendkracht binnen de instelling werkzaam is. Hierbij kan gedacht worden aan uitzendkrachten, vergoedingen voor freelance bezigheidstherapeuten, consulenten, medisch adviseurs, docenten en dergelijke, maar ook voor medisch specialisten die niet in loondienst van de instelling werken. Denk bijvoorbeeld ook aan kappers en assistenten voor de eredienst en dergelijke.

Bij vergoedingen is geen sprake van een dienstbetrekking. De instelling is niet inhoudingsplichtig.
Het personeel voert de door haar opgedragen werkzaamheden echter wel uit onder de (directe) leiding van de instelling, en maakt daarbij veelal gebruik van faciliteiten, materialen en gereedschappen van de instelling.

Voorbeelden hiervan zijn:

- uitzendkrachten, stagiaires en freelance medewerkers;
- oproep- en invalkrachten voor zover de instelling daarvoor niet inhoudingsplichtig is;
- medische en andere specialisten niet in loondienst;

Bij uitbestede werkzaamheden worden werkzaamheden niet uitgevoerd onder leiding van de instelling. Hiervoor koopt de instelling diensten in en geeft aan derden opdracht tot uit voering en leiding van dergelijke werkzaamheden (Bron: Rekeningschema-voor-zorginstellingen-Prismant - Utrecht, september 2004 Publicatienummer 203.25).

Ter illustratie staan in onderstaande tabellen de verwijzingen naar de betreffende grootboekrekeningnummers conform het Referentiegrootboekschema (RGS) en het Prismant rekeningschema.

| Specificatie rubrieken RGS|
|----------------|
| WBedOvpUik           W.B.A       4012010      Uitzendkrachten    Uitzendkrachten overige personeelskosten |
| WBedOvpUikUik        W.B.A010    4012010.01    Uitzendkrachten    Uitzendkrachten overige personeelskosten |
| WBedOvpUikFor        W.B.A020    4012010.02    Uitzendkrachten  formatief    Uitzendkrachten  formatief |
| WBedOvpUikPrj        W.B.A030    4012010.03    Uitzendkrachten projectmatig    Uitzendkrachten projectmatig |
| WBedOvpUikBfo        W.B.A040    4012010.04    Uitzendkrachten boven formatief    Uitzendkrachten boven formatief |
| WBedOvpUikPro        W.B.A050    4012010.05    Uitzendkrachten programma's    Uitzendkrachten programma's |
| WBedOvpUit           W.B.C       4012020    Uitzendbedrijven    Uitzendbedrijven overige personeelskosten |
| WBedOvpMaf           W.B.D       4012030    Management fee    Management fee overige personeelskosten |
| WBedOvpZzp           W.B.E       4012040    Ingehuurde ZZP-ers    Ingehuurde ZZP-ers overige personeelskosten |
| WBedOvpPay           W.B.F       4012050    Ingehuurde payrollers    Ingehuurde payrollers overige personeelskosten |
| WBedOvpOip           W.B.G       4012060    Overig ingeleend personeel    Overig ingeleend personeel overige personeelskosten |

| Specificatie rubrieken Prismant|
|----------------|
| 418000 Vergoedingen voor niet in loondienst verrichte arbeid |
| 418100 Uitzendkrachten |
| 418200 Overig personeel niet in loondienst |

## Uitgangspunten

- Alle kosten van alle personeelsleden niet in loondienst (PNIL) worden geïncludeerd.

## Berekening

Deze indicator wordt als volgt berekend:

1. Tel alle boekingen van kosten van personeel niet in loondienst (PNIL) voor de betreffende meetperiode bij elkaar op.

| Periode:       | Totaal kosten PNIL (Euro)|
|----------------|--------|
| Totaal organisatie | Stap 1 |
