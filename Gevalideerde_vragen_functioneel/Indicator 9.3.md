---
title: 9.3. Wat zijn de energiekosten (Gas-elektriciteit-water)?
description: "Het totaal aan energiekosten."
weight: 9
---
## Indicator

**Definitie:** Het totaal aan energiekosten.

**Teller:** Totaal aan energiekosten.

**Noemer:** Niet van Toepassing.

## Toelichting

Deze indicator betreft het totaal aan energiekosten over de periode.
De indicator wordt op organisatieniveau berekend.

Voor totaal aan energiekosten hanteert de NZa het Prismant rekeningsschema (Bron: Kostenonderzoek Wet langdurige zorg NZa en KPMG — Versie 12 september 2017, pagina 18).

In het Prismant rekeningschema zijn dit alle rekeningen uit rekeninggroep 473 uit rubriek 47 (Bron: Rekeningschema-voor-zorginstellingen-Prismant - Utrecht, september 2004 Publicatienummer 203.25, pagina's 38 en 62).

Ter illustratie staan in onderstaande tabellen de verwijzingen naar de betreffende grootboekrekeningnummers conform het Referentiegrootboekschema (RGS) en het Prismant rekeningschema.

| Specificatie rubrieken RGS|
|----------------|
| WBedHuiGwe 4201105 Gas,water en elektra (algemeen) |
| WBedHuiGas 4201141 Energiekosten gas |
| WBedHuiWat 4201142 Energiekosten water |
| WBedHuiElk 4201143 Energiekosten elektra |
| WBedHuiTrg 4201144 Teruglevering elektra |

| Specificatie rubrieken Prismant|
|----------------|
| 473000 Energiekosten |
| 473100 Olie |
| 473200 Kolen |
| 473300 Elektriciteit |
| 473400 Gas |
| 473500 Stadsverwarming |
| 473600 Water |
| 473900 Andere kosten van energie |

## Uitgangspunten

* Alle energiekosten worden geincludeerd.

## Berekening

Deze indicator wordt als volgt berekend:

1. Tel alle boekingen van energiekosten voor de betreffende meetperiode bij elkaar op.

| Periode:       | Totaal energiekosten kosten (Euro)|
|----------------|--------|
| Totaal organisatie | Stap 1 |
