---
title: 6.1. Wat zijn de vervoerskosten?
description: "Totaal aan kosten van vervoer."
weight: 6
---
## Indicator

**Definitie:** Totaal aan kosten van vervoer.

**Teller:** Totaal aan kosten van vervoer.

**Noemer:** Niet van Toepassing.

## Toelichting

Deze indicator betreft het totaal aan alle vervoerskosten die verband houden met cliëntvervoer over de periode (Bron: Gebruikersinstructie Kostenonderzoek 2016 - Kostenonderzoek Wet langdurige zorg NZa en KPMG — Versie 12 september 2017, pagina 17).

De indicator wordt op organisatieniveau berekend.

Voor de berekening van deze indicator hanteert de NZa het Prismant rekeningsschema (Bron: Rekeningschema-voor-zorginstellingen-Prismant - Utrecht, september 2004 Publicatienummer 203.25, pagina 35).

Ter illustratie staan in onderstaande tabellen de verwijzingen naar de betreffende rubrieken conform het Prismant rekeningschema en het Referentiegrootboekschema (RGS).

| Specificatie rubrieken RGS |
|----------------|
| Niet geïmplementeerd vanuit RGS |

| Specificatie rubrieken Prismant |
|----------------|
| 443000 Vervoerskosten |
| 443100 Intern vervoer |
| 443200 Extern vervoer (niet nacalculeerbaar) |
| 443300 Extern vervoer (nacalculeerbaar) |

## Uitgangspunten

* Alle vervoerskosten die verband houden met cliëntvervoer worden geincludeerd.

## Berekening

Deze indicator wordt als volgt berekend:

1. Tel alle boekingen van vervoerskosten voor de betreffende meetperiode bij elkaar op.

| Periode:       | Totaal vervoerskosten (Euro)|
|----------------|--------|
| Totaal organisatie | Stap 1 |
