---
title: 17.2 Goedgekeurd gedeclareerd bedrag per langdurige zorg sector
weight: 17
---

## Toelichting

Deze indicator betreft het bedrag dat door de zorgaanbieder per sector in de langdurige zorg voor zorg is gedeclareerd. De indicator wordt berekend op organisatieniveau voor een bepaalde periode. Het betreft goedgekeurde declaraties.


## Uitgangspunten

* Uitsluitend 'goedgekeurde declaraties' worden geïncludeerd.
* De goedgekeurde declaratie wordt geïncludeerd wanneer de datum waarop of periode waarin de zorg geleverd is, in de verslagperiode ligt.
* De indeling van een goedgekeurde declaratie in een bepaalde categorie vindt plaats op basis van de prestatiecode. Een beschrijving van de manier waarop dit plaatsvindt staat nader beschreven in de Algemene uitgangspunten in de paragraaf 'Het gebruik van declaraties'.


## Berekening

De indicator wordt als volgt berekend:

1. Selecteer alle goedgekeurde declaraties waarvan de datum waarop of periode waarin de zorg geleverd is in de verslagperiode ligt.
2. Bepaal op basis van stap 1 per declaratie het gedeclareerde bedrag en de langdurige zorgsector waaronder de declaratie valt.
3. Bereken het gedeclareerde bedrag in Euro per sector en voor alle sectoren tezamen.

Periode: dd-mm-jjjj tot en met dd-mm-jjjj
| Organisatie-onderdeel |  Gedeclareerd bedrag GGZ-B | Gedeclareerd bedrag LG | Gedeclareerd bedrag SGLVG | Gedeclareerd bedrag LVG overig |Gedeclareerd bedrag VG | Gedeclareerd bedrag ZGAUD | Gedeclareerd bedrag ZGVIS | Gedeclareerd bedrag VV | Overig in de Wlz | 
|----------------|--------|-----------|--------|--------|--------|-----------|--------|--------|--------|
| Totaal organisatie | Stap 3 | Stap 3 | Stap 3 | Stap 3 | Stap 3 | Stap 3 | Stap 3 | Stap 3 | Stap 3 |

