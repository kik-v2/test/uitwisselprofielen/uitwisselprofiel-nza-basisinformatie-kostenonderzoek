---
title: 9.1. Wat zijn de terrein- en gebouwgebonden kosten?
description: "Het totaal aan terrein- en gebouwgebonden kosten."
weight: 9
---
## Indicator

**Definitie:** Het totaal aan terrein- en gebouwgebonden kosten.

**Teller:** Totaal aan terrein- en gebouwgebonden kosten.

**Noemer:** Niet van Toepassing.

## Toelichting

Deze indicator betreft het totaal aan terrein- en gebouwgebonden kosten over de periode.

De indicator wordt op organisatieniveau berekend.

Voor terrein- en gebouwgebonden kosten hanteert de NZa het Prismant rekeningsschema (Bron: Kostenonderzoek Wet langdurige zorg NZa en KPMG — Versie 12 september 2017, pagina 18).

In het Prismant rekeningschema zijn dit alle rekeningen uit rekeninggroep 471 uit rubriek 47 (Bron: Rekeningschema-voor-zorginstellingen-Prismant - Utrecht, september 2004 Publicatienummer 203.25, pagina's 38 en 62). In deze rekeninggroep worden de materiële kosten verantwoord die gemaakt worden voor het onderhoud van terreinen, gebouwen en installaties. De onderhoudskosten van onder andere apparatuur en instrumentarium worden geboekt in de desbetreffende rekeninggroepen binnen de rubrieken 43, 44, 45 en 46. De kosten van jaarlijks terugkerend onderhoud worden geboekt in de rekeninggroep 471.

Ter illustratie staan in onderstaande tabellen de verwijzingen naar de betreffende grootboekrekeningnummers conform het Referentiegrootboekschema (RGS) en het Prismant rekeningschema.

| Specificatie rubrieken RGS|
|----------------|
| WBedHuiOnt 4201070 Onderhoud terreinen |
| WBedHuiOng 4201080 Onderhoud gebouwen |
| WBedEem 4202000 Exploitatie- en machinekosten |

| Specificatie rubrieken Prismant|
|----------------|
| 471100 Onderhoud terreinen |
| 471200 Onderhoud gebouwen |
| 471300 Onderhoud installaties |
| 471400 Materialen, machines en gereedschappen t.b.v. onderhoud Dotatie groot onderhoud |

## Uitgangspunten

* Alle terrein- en gebouwgebonden kosten worden geincludeerd.

## Berekening

Deze indicator wordt als volgt berekend:

1. Tel alle boekingen van terrein- en gebouwgebonden kosten voor de betreffende meetperiode bij elkaar op.

| Periode:       | Totaal terrein- en gebouwgebonden kosten (Euro)|
|----------------|--------|
| Totaal organisatie | Stap 1 |
