---
title: 1.1. Ziekteverzuimpercentage (excl. zwangerschapsverlof)
description: "Het ziekteverzuimpercentage is het totaal aantal ziektedagen van personeelsleden in loondienst (PIL), in procenten van het totaal aantal beschikbare kalenderdagen van PIL in de meetperiode.(excl. zwangerschapsverlof)"
weight: 1
---
## Indicator

**Definitie:** Het ziekteverzuimpercentage is het totaal aantal ziektedagen van personeelsleden in loondienst (PIL), in procenten van het totaal aantal beschikbare kalenderdagen van PIL in de meetperiode. Het ziekteverzuimpercentage is exclusief zwangerschaps- en bevallingsverlof. In de berekening wordt rekening gehouden met de parttimefactor.

**Teller:** Aantal ziektedagen.

**Noemer:** Aantal kalenderdagen.

## Toelichting

Het ziekteverzuimpercentage is het totaal aantal ziektedagen van PIL ten op zichte van het totaal aantal beschikbare kalenderdagen (dus geen werkdagen) van PIL per jaar. Het ziekteverzuimpercentage is exclusief zwangerschaps- en bevallingsverlof. In de berekening wordt rekening gehouden met de parttimefactor en het arbeidsongeschiktheidspercentage.

## Uitgangspunten

* Alle PIL (zorg- en niet-zorg) worden geïncludeerd.
* Zwangerschaps- en bevallingsverlof worden uitgesloten.
* Alle ziekteperiodes (kort en lang) worden geïncludeerd.
* In de berekening wordt de parttimefactor geïncludeerd.
* 1 ziektedag = 1 kalenderdag waarop een PIL ziek is.
* Wanneer het arbeidsongeschiktheidspercentage bij de verzuimmelding ontbreekt, wordt het arbeidsongeschiktheidspercentage verondersteld 100% te zijn.
* Voor de berekening van deze indicator is het noodzakelijk dat in de administratie van de zorgaanbieder wijzigingen in parttime factor, vestiging en functie gedurende de meetperiode beschikbaar zijn.
* Indien een ziekteperiode na de einddatum van de meetperiode doorloopt of nog geen eindatum heeft dan wordt de einddatum van de meetperiode gebruikt in de berekening als einddatum van de ziekteperiode (Dus indien de ziekteperiode van 20 oktober in het 1e jaar tot en met 15 april in het 2e jaar is en de berekening gaat over het 1e jaar dan wordt een einddatum van de ziekteperiode van 31 december van het 1e jaar gehanteerd in de berekening). Dit is alleen van toepassing op een eindatum van een ziekteperiode in de berekening. De startdatum van een ziekteperiode wordt niet aangepast. Vergelijkbaar wordt voor de ziekteperiode een startdatum van 1 januari gebruikt als in het voorgaande voorbeeld de berekening het tweede jaar betreft.

## Berekening

De indicator wordt als volgt berekend (zie tevens onderstaande tabel):

1. Selecteer alle PIL (o.b.v. de arbeidsovereenkomst) in de betreffende meetperiode.
2. Bepaal o.b.v. stap 1 per persooneelslid per arbeidsovereenkomst voor elke dag binnen de meetperiode de vestiging, functie en parttimefactor.
3. Selecteer van alle PIL uit stap 1 de ziekteperiodes exclusief zwangerschaps- en bevallingsverlof.
4. Bepaal o.b.v. stap 3 per PIL per arbeidsovereenkomst voor elke dag binnen de ziekteperiode het ziektepercentage.
5. Bereken o.b.v. stap 2 en stap 4 per PIL per arbeidsovereenkomst voor elke dag binnen de meetperiode het ziekteverzuim door de parttimefactor te vermenigvuldigen met het ziektepercentage.
6. Pas per PIL per arbeidsovereenkomst voor elke dag binnen de meetperiode de parttimefactor toe op de kalenderdag.
7. Bereken voor de totale organisatie het verzuimpercentage door het ziekteverzuim van de betreffende PIL bij elkaar op te tellen en te delen door de optelling van het aantal kalenderdagen waarop de parttimefactor is toegepast. Vermenigvuldig het resultaat met 100%.

Periode: dd-mm-jjjj tot en met dd-mm-jjjj

| Organisatieonderdeel       |  Ziekteverzuimpercentage  |
|----------------|--------|
| Totaal organisatie | Stap 7 |
