---
title: 10.3. Wat zijn huur en afschrijvingen inventaris en gebouwen, financieringskosten en dotaties?
description: "Het totaal aan huur en afschrijvingen inventaris en gebouwen, financieringskosten en dotaties."
weight: 10
---
## Indicator

**Definitie:** Het totaal aan huur en afschrijvingen inventaris en gebouwen, financieringskosten en dotaties.

**Teller:** Totaal kosten huur en afschrijvingen inventaris en gebouwen, financieringskosten en dotaties.

**Noemer:** Niet van Toepassing.

## Toelichting

Deze indicator betreft het totaal aan huur en afschrijvingen inventaris en gebouwen, financieringskosten en dotaties over de periode. De indicator wordt op organisatieniveau berekend.

Voor huur en afschrijvingen inventaris en gebouwen, financieringskosten en dotaties hanteert de NZa het Prismant rekeningsschema (Bron: Kostenonderzoek Wet langdurige zorg NZa en KPMG — Versie 12 september 2017, pagina 18).

In het Prismant rekeningschema zijn dit de huur en afschrijvingen inventaris en gebouwen, financieringskosten en dotaties die vallen onder rubriek 48 (Bron: Rekeningschema-voor-zorginstellingen-Prismant - Utrecht, september 2004 Publicatienummer 203.25, pagina 38 en 39).

Ter illustratie staan in onderstaande tabellen de verwijzingen naar de betreffende grootboekrekeningnummers conform het Referentiegrootboekschema (RGS) en het Prismant rekeningschema.

| Specificatie rubrieken RGS |
|----------------|
| Nader uit te zoeken (verspreid door RGS aanwezig) |

| Specificatie rubrieken Prismant |
|----------------|
| Alle rubrieken met betrekking tot 480000 exclusief de volgende rubrieken: 480012; 480022; 480032; 480112; 480122; 480132; 480532; 480632; 480932; 484222; 484232; 486725; 486735  |

## Uitgangspunten

* Alle huur en afschrijvingen inventaris en gebouwen, financieringskosten en dotaties worden geincludeerd.

## Berekening

Deze indicator wordt als volgt berekend:

1. Tel alle boekingen voor kosten huur en afschrijvingen inventaris en gebouwen, financieringskosten en dotaties voor de betreffende meetperiode bij elkaar op.

Periode: dd-mm-jjjj tot en met dd-mm-jjjj

| Organisatieonderdeel       | Totaal kosten aan huur en afschrijvingen inventaris en gebouwen, financieringskosten en dotaties (Euro)|
|----------------|--------|
| Totaal organisatie | Stap 1 |
