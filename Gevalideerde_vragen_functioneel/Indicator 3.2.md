---
title: 3.2. Gemiddeld bruto jaarsalaris per fte
description: "Het gemiddelde bruto jaarsalaris per contractuele fte."
weight: 3
---
## Indicator

**Definitie:** Het gemiddelde bruto jaarsalaris per contractuele fte.

**Teller:** Bruto jaarsalaris PIL.

**Noemer:** Aantal contractuele fte PIL.

## Toelichting

Deze indicator betreft het gemiddelde bruto jaarsalaris van personeel in loondienst (PIL) over de periode inclusief vakantiegeld, vergoedingen (zoals eindejaarsuitkering) en sociale lasten per fte. De indicator wordt op organisatieniveau berekend.

Bruto jaarsalaris betreft het totaal betaalde salaris, inclusief sociale lasten, toeslagen en uitkeringen (Bron: Kostenonderzoek Wet langdurige zorg NZa en KPMG - Versie 12 september 2017).

## Uitgangspunten

* Alle personeelsleden (zorg- en niet-zorg) worden geïncludeerd.
* Alle arbeidsovereenkomsten van alle personeelsleden tellen mee.
* Er wordt uitgegaan van contractuele fte's.
* 1 fte is 36 uren per week.

## Berekening

Deze indicator wordt als volgt berekend (zie tevens onderstaande tabel):

1. Selecteer alle personeelsleden (o.b.v. de werkovereenkomst) die personeelslid waren in het betreffende jaar.
2. Bepaal per arbeidsovereenkomst het totaal aantal contractuele fte in het betreffende jaar naar rato van de duur van de arbeidsovereenkomst.
3. Tel (o.b.v. stap 2 ) alle contractuele fte's bij elkaar op (noemer).
4. Tel alle boekingen van loonkosten van personeel in loondienst (PIL) voor de betreffende meetperiode bij elkaar op (Zie indicator NZa 3.3 Loonkosten voor personeel in loondienst (PIL)).
5. Deel de teller (stap 4) door de noemer (stap 3).

| Periode:       | Totaal bruto jaarsalaris | Totaal aantal contractuele fte | Gemiddelde bruto jaarsalaris per fte |
|----------------|--------| ---| ---|
| Totaal organisatie | Stap 4 | Stap 3 | Stap 5 |
